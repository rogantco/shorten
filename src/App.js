import React, { Component } from 'react';
import Header from './components/header';
import Home from './scenes/Home';

class App extends Component {
  render() {
    return (
      <div className='app'>
        <Header />
        <Home />
      </div>
    );
  }
}

export default App;
