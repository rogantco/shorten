import React from 'react';
import styles from './styles';

const Header = () => {
  return (
    <div style={styles.container}>
      <h1 style={styles.title}>Shooooort</h1>
      <div style={styles.slogan}>The link shortener with a long name</div>
    </div>
  )
};

export default Header;
