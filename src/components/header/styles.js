const styles = {
  container: {
    display: 'flex',
    marginTop: 100,
  },
  title: {
    color: '#eb4a42',
    flex: '1',
    fontFamily: '"Montserrat", sans-serif',
    fontSize: 46,
    margin: 0,
    textDecoration: 'underline',
  },
  slogan: {
    alignSelf: 'center',
    color: '#aaaaaa',
    flex: '1',
    fontFamily: '"Roboto", sans-serif',
    fontSize: 16,
    marginTop: 10,
    textAlign: 'right',
  }
};

export default styles;
