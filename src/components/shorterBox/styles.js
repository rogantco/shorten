const styles = {
  container: {
    marginTop: 55
  },
  field: {
    background: '#eaeaea',
    border: 0,
    borderRadius: 3,
    boxSizing: 'border-box',
    color: '#555555',
    fontFamily: '"Roboto", sans-serif',
    fontSize: 16,
    height: 42,
    outline: 'none',
    padding: '0 18px',
    width: 457,
  },
  button: {
    background: '#e0e0e0',
    borderRadius: 3,
    border: 0,
    color: '#bfbfbf',
    fontFamily: '"Roboto", sans-serif',
    fontSize: 16,
    marginLeft: 15,
    height: 42,
    verticalAlign: 'top',
    width: 148,
  },

  buttonActive: {
    background: '#eb4a42',
    color: '#fff',
    cursor: 'pointer',
    outline: 'none'
  }
};

export default styles;
