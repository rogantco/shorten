import React, { Component } from 'react';
import styles from './styles';

class ShorterBox extends Component {
  state = {
    url: ''
  };

  urlHandleChange = (e) => {
    this.setState({url: e.target.value});
  };

  sendHandler = () => {
    this.props.onUrlChange(this.state.url);
    this.setState({url: ''});
  };

  isButtonDisabled = () => {
    const regex = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/g;
    const invalidUrl = this.state.url.match(regex) === null;

    return this.state.url.length === 0 || invalidUrl
  };

  render() {
    return (
      <div style={styles.container}>
        <input style={styles.field} type="text" onChange={this.urlHandleChange} value={this.state.url}/>
        <button
          style={this.isButtonDisabled() ? styles.button : Object.assign({}, styles.button, styles.buttonActive)}
          disabled={this.isButtonDisabled()}
          onClick={this.sendHandler}
        >Shorten this link</button>
      </div>
    );
  }
}

export default ShorterBox;
