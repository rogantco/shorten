import React, { Component } from 'react';
import Moment from 'react-moment';
import styles from './styles';

class Link extends Component {
  state = {
    onHover: false,
    urlCopied: false,
  };

  mouseOverHandler = () => {
    this.setState({onHover: true})
  };

  mouseLeaveHandler = () => {
    this.setState({onHover: false});
    this.setState({urlCopied: false});
  };

  copyLink = () => {
    const textArea = document.createElement('input');
    textArea.value = `https://impraise-shorty.herokuapp.com/${this.props.link.hash}`;
    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();

    try {
      const successful = document.execCommand('copy');

      if(successful) {
        this.setState({urlCopied: true})
      }
    } catch (err) {
      console.error('Fallback: Oops, unable to copy', err);
    }

    document.body.removeChild(textArea);
  };

  renderCopyCTA () {
    if(this.state.onHover && !this.state.urlCopied) {
      return (
        <span style={styles.container.shortedUrl.copy} onClick={this.copyLink}>Click to copy this link</span>
      )
    }

    if(this.state.onHover && this.state.urlCopied) {
      return (
        <span style={styles.container.shortedUrl.copy}>Copied to the clipboard</span>
      )
    }
  }

  render() {
    const {link} = this.props;

    return (
      <div style={styles.container} onMouseOver={this.mouseOverHandler} onMouseLeave={this.mouseLeaveHandler}>
        <div style={styles.container.shortedUrl}>
          <div>
            <a style={styles.container.shortedUrl.link} href={`https://impraise-shorty.herokuapp.com/${link.hash}`} target="_blank">
              shooooort.com/<span style={styles.container.shortedUrl.hash}>{link.hash}</span>
            </a>
            {this.renderCopyCTA()}
          </div>
          <div style={styles.container.shortedUrl.fullLink}>{link.url}</div>
        </div>
        <div style={styles.container.visits}>{link.count}</div>
        <div style={styles.container.last}>
          {link.lastSeenDate ? [<Moment toNow ago key={link.hash}>{link.lastSeenDate}</Moment>, ' ago'] : 'No visits yet'}
          </div>
      </div>
    );
  }
}

export default Link;
