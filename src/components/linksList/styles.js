const styles = {
  root: {
    marginTop: 5,
  },
  container: {
    display: 'flex',
    marginTop: 40,

    shortedUrl: {
      flex: 6,
      textAlign: 'left',
      color: '#aaaaaa',
      fontFamily: '"Roboto", sans-serif',
      fontSize: 14,

      link: {
        color: '#555555',
        fontFamily: '"Roboto", sans-serif',
        fontSize: 16,
        textDecoration: 'none'
      },
      hash: {
        color: '#eb4a42'
      },
      fullLink: {
        fontFamily: '"Roboto", sans-serif',
        fontSize: 16,
        width: 380,
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        marginTop: 15,
      },
      copy: {
        color: '#eb4a42',
        cursor: 'pointer',
        fontFamily: '"Roboto", sans-serif',
        fontSize: 16,
        marginLeft: 20,
      },
    },
    visits: {
      alignSelf: 'center',
      flex: 1,
      textAlign: 'right',
      color: '#aaaaaa',
      fontFamily: '"Roboto", sans-serif',
      fontSize: 14,
    },
    last: {
      alignSelf: 'center',
      flex: 2,
      marginLeft: 35,
      textAlign: 'right',
      color: '#aaaaaa',
      fontFamily: '"Roboto", sans-serif',
      fontSize: 14,
    },
  }
};

export default styles;
