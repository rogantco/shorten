import React from 'react';
import Link from './link';
import styles from './styles';

const renderLinks = (links) => {
  return links.map((link) => {
    return (
      <Link link={link} key={link.hash} />
    )
  })
};

const LinkList = (props) => {
  return (
    <div style={styles.root}>
      <div style={styles.container}>
        <div style={styles.container.shortedUrl}>LINK</div>
        <div style={styles.container.visits}>VISITS</div>
        <div style={styles.container.last}>LAST VISITED</div>
      </div>
      {renderLinks(props.links)}
    </div>
  )
};

export default LinkList;
