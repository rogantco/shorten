const styles = {
  previously : {
    color: '#555555',
    fontFamily: '"Roboto", sans-serif',
    fontSize: 22,
    marginBottom: 0,
    marginTop: 80,
  },
  clearButton: {
    color: '#eb4a42',
    cursor: 'pointer',
    fontFamily: '"Roboto", sans-serif',
    fontSize: 16,
    marginLeft: 25,
    textDecoration: 'none',
  }
};

export default styles;
