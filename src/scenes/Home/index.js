import React, { Component } from 'react';
import styles from './styles';
import ShorterBox from './../../components/shorterBox';
import LinkList from './../../components/linksList';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shortedUrls: localStorage.getItem('shortedUrls') ? JSON.parse(localStorage.getItem('shortedUrls')) : []
    };
  }

  componentDidMount () {
    this.state.shortedUrls.map((url, index) => {
      return fetch(`/${url.hash}/stats`).then((response) => response.json()).then((responseJson) => {
        const link = {
          ...url,
          count: responseJson.redirectCount,
          startDate: responseJson.startDate,
          lastSeenDate: responseJson.lastSeenDate || '',
        };

        this.setState({shortedUrls: Object.assign([], this.state.shortedUrls, {[index]: link})}, () => {
          localStorage.setItem('shortedUrls', JSON.stringify(this.state.shortedUrls))
        })
      })
    });
  }

  shortUrl = (url) => {
    fetch('/shorten', {
        method: 'POST',
        body: JSON.stringify({url: url})
      }
    ).then((response) => response.json()).then((responseJson) => {
      this.setState({shortedUrls: [].concat([{ hash: responseJson.shortcode, url: url}]).concat(this.state.shortedUrls)}, () => {
        localStorage.setItem('shortedUrls', JSON.stringify(this.state.shortedUrls))
      })
    })
  };

  clearHistory = () => {
    this.setState({shortedUrls: []});
    localStorage.setItem('shortedUrls', '[]');
  };

  render() {
    return (
      <div>
        <ShorterBox onUrlChange={this.shortUrl} />
        <p style={styles.previously}>
          Previously shortened by you
          <a style={styles.clearButton} onClick={this.clearHistory}>Clear history</a>
        </p>
        <LinkList links={this.state.shortedUrls} />
      </div>
    );
  }
}

export default Home;
